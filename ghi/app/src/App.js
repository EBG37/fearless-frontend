import Nav from './Nav'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './NewConference'
import AttendConferenceForm from './AttendConference';
import PresentationForm from './PresentationForm'
import ConferenceColumn from './MainPage'
import {createBrowserRouter, RouterProvider, Outlet} from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
console.log(props.attendees)
  const router = createBrowserRouter([
    {
      path: '/',
      element: (
        <>
          <Nav />
          <div className="container">
            <Outlet />
          </div>
        </>
      ),
      children: [
        {
          path: "locations",
          children: [
            { path: "new", element: <LocationForm /> },
          ]
        },
        {
          path: "conferences",
          children: [
            {path: 'new', element: <ConferenceForm /> },
          ]
        },
        {
          path: "attendees",
          children: [
            {path: '', element: <AttendeesList attendees={props.attendees} /> },
            {path: 'new', element: <AttendConferenceForm /> },
          ]
        },
        {
          path: "presentations",
          children: [
            {path: 'new', element: <PresentationForm /> },
          ]
        },
        {
          path: "MainPage",
          children: [
            {index: true,  element: <ConferenceColumn /> }
          ]
        }
      ],
    },
  ])
  return <RouterProvider router={router} />;

  // return (
  //   <>
  //     <Nav />
  //   <div className="container">
  //     <AttendConferenceForm />
  //     {/* <ConferenceForm /> */}
  //     {/* <LocationForm /> */}
  //     {/* <AttendeesList attendees={props.attendees} /> */}
  //   </div>
  //   </>
  // );
}

export default App;
