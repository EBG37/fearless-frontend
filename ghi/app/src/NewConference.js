import React, { useEffect, useState } from 'react'

function ConferenceForm(props) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location


        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('')
          setStart('')
          setEnd('')
          setDescription('')
          setMaxPresentations('')
          setAttendees('')
          setLocation('')
        }
      }

    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    const [name, setName] = useState('')
    const [starts, setStart] = useState('')
    const [ends, setEnd] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setAttendees] = useState('')
    const [location, setLocation] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
      }
      const handleStartChange = (event) => {
        const value = event.target.value
        setStart(value)
      }
      const handleEndChange = (event) => {
        const value = event.target.value
        setEnd(value)
      }
      const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
      }
      const handlePresentationChange = (event) => {
        const value = event.target.value
        setMaxPresentations(value)
      }
      const handleAttendeesChange = (event) => {
        const value = event.target.value
        setAttendees(value)
      }
      const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
      }

    useEffect(() => {
        fetchData()
    }, [])

    return(
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" value={name} className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartChange} placeholder="Starts" required type="datetime-local" name="starts" value={starts} id="starts" className="form-control" />
                  <label htmlFor="starts">Start date</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndChange} placeholder="ends" required type="datetime-local" name="ends" value={ends} id="ends" className="form-control" />
                  <label htmlFor="ends">End date</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleDescriptionChange} placeholder="description" required type="text" name="description" value={description} id="description" className="form-control" />
                  <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePresentationChange} placeholder="max_presentations" required type="number" name="max_presentations" value={maxPresentations} id="max_presentations" className="form-control" />
                  <label htmlFor="max_presentations">Max presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" value={maxAttendees} id="max_attendees" className="form-control" />
                  <label htmlFor="max_attendees">Max attendees</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required name="location" value={location} id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                      return (
                          <option key={location.name} value={location.name}>
                          {location.name}
                          </option>
                      );
                      })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default ConferenceForm